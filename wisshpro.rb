#!/usr/bin/env ruby
#
#   1. Analyzes ssh parameters to infer hostname and port
#   2. Adds prefix, suffix, and scans for the best host in this port
#   3. Calls ssh
#
#   by Alfrenovsky
#

require 'resolv'

DEFAULT_PORT=22000
HOST_SUFFIX="sequreisp.com"
HOST_PREFIXES=(0..50)
PORT_SCAN_TIMEOUT=10

class WisproHost
  attr_reader :hostname

  def initialize(hostname,port)
    @hostname=hostname
    @port=port
    @t=Thread.new{measure}
  end

  def rank
    # Resolution thread was launched on initialize
    # Now we wait for the thread to end
    @t.join
    return nil unless @answers
    return @latency
  end

  private

  def measure
    return nil unless ip
    @latency ||= (
      start=Time.now
      @answers=(not File.read("| nmap #{hostname} -P0 --host-timeout #{PORT_SCAN_TIMEOUT} -p #{@port.to_s}").scan(/open/).flatten.empty?)
      Time.now-start )
  end

  def ip
    @ip ||= (Resolv.getaddress(hostname).inspect rescue nil)
  end
end

# Theese flags needs another parameter, could be the next parameter or
# the string inmediatly next (without space)
flags_with_param=%w(b c D E e F I i L l m O o p Q q R S W w)
# Assuming any other flag if boolean

params_kv={} # Key value type parameters
params_w=[] # word params (hostname and commands)

args=ARGV.clone

args.each_index do |i|
  param=args[i]
  key=value=nil
  if param
    if param[0] == "-"
      key=param[1]
      if key
        if flags_with_param.include?(key)
          if param[2]
            value=param[2..-1]
          else
            value=args[i+1]
            args[i+1]=nil
          end
        else
          value=true
        end
      end
      params_kv[key]=value
    else
      params_w << param
    end
  end
end

hostname=params_w[0].gsub(/[^@]*@/,"") if params_w[0]
port=(params_kv["p"] || DEFAULT_PORT)

# The hosts array is initialized first to allow threaded resolution
hosts=HOST_PREFIXES.map{|h| WisproHost.new("#{h.to_s}.#{hostname}.#{HOST_SUFFIX}",port)}

# Then selects working hosts and finds best (lower lattency)
best=hosts.select{|h| not h.rank.nil?}.sort{|x,y| x.rank <=> y.rank}.first

if best
  if port == "80" or port == "8080" or port == "443"
    protocol = (port == "443" ? 'https' : 'http')
    params=["xdg-open"]+["#{protocol}://#{best.hostname}:#{port}/oauth"]
  else
    puts "Logging in to #{best.hostname}\n\n"
    params=["ssh"]+"-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no".split+ARGV.map{|a| a.gsub(hostname,best.hostname)}
  end
  system(*params)
else
  # Bad luck
  puts "No host for #{hostname}, from #{HOST_PREFIXES.first} to #{HOST_PREFIXES.last}, resolvs and answers in port #{port}."
end
