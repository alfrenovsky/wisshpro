# README #

This script can find best wispro server for ssh

### How does it works? ###

1. You call wisshpro with the same parameters you call ssh
2. Wisshpro generates hostnames adding prefixes from 0 to 50 and a suffix. 
3. Resolves each hostname and tries to reach the ssh port
4. If succeeded uses the hostname with best latency and calls ssh or
   call xdg-open with http or http when port is 80, 8080 or 443  

It should last at most:

* DNS timeout time + 10 seconds.

It will only lasts those 10 seconds if some host has the port filtered. If all the hosts have the port accepted or rejected the time should be at most:

* DNS timeout time + worse host ssh port latency

### To do ###

* Some caching with due date to avoid the need for re-measuring each time.

### Install ###

cd /usr/src

git clone https://alfrenovsky@bitbucket.org/alfrenovsky/wisshpro.git

ln -fs /usr/src/wisshpro/wisshpro.rb /usr/local/bin/wisshpro

